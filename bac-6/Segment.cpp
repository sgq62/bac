/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/Segment.cpp
 * @author Stephan Kreutzer
 * @since 2020-10-24
 */


#include <sstream>
#include "Segment.h"


namespace bac
{

Segment::Segment(const std::string& strString):
  m_strString(strString)
{
    if (strString.empty() == true)
    {
        throw new std::invalid_argument("Segment::Segment(): Empty string passed.");
    }
}

std::string Segment::GetString(const std::vector<std::unique_ptr<std::string>>& aArguments)
{
    if (m_pHoles == nullptr ||
        aArguments.size() <= 0)
    {
        return m_strString;
    }

    std::stringstream strResult;
    std::size_t nStart = 0L;

    for (std::map<std::size_t, std::size_t>::iterator iter = m_pHoles->begin();
         iter != m_pHoles->end();
         iter++)
    {
        strResult << m_strString.substr(nStart, iter->first - nStart);

        if (iter->second < aArguments.size())
        {
            strResult << *(aArguments.at(iter->second));
        }

        nStart = iter->first;
    }

    strResult << m_strString.substr(nStart, std::string::npos);

    return strResult.str();
}

int Segment::SetString(const std::string& strString)
{
    m_strString = strString;

    if (m_pHoles != nullptr)
    {
        m_pHoles.reset(nullptr);
    }

    return 0;
}

int Segment::DrillHoles(const std::list<std::unique_ptr<std::string>>& aStrings)
{
    if (aStrings.empty() == true)
    {
        throw new std::invalid_argument("Segment::DrillHoles(): No arguments.");
    }

    m_pHoles.reset(new std::map<std::size_t, std::size_t>());

    int nCount = 0L;

    for (std::list<std::unique_ptr<std::string>>::const_iterator iter = aStrings.begin();
         iter != aStrings.end();
         iter++)
    {
        if ((*iter)->empty() == true)
        {
            throw new std::invalid_argument("Segment::DrillHoles(): Empty string passed.");
        }

        std::size_t nPos = m_strString.find(**iter);

        if (nPos == std::string::npos)
        {
            std::stringstream aMessage;
            aMessage << "String \"" << **iter << "\" not found in segment \"" << m_strString << "\".";
            throw new std::runtime_error(aMessage.str());
        }

        do
        {
            m_strString.erase(nPos, (*iter)->length());

            m_pHoles->insert(std::pair<std::size_t, long>(nPos, nCount));

            ++nCount;
            nPos = m_strString.find(**iter, nPos);

        } while (nPos != std::string::npos);
    }

    return 0;
}

}
