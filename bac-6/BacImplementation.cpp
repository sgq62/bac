/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/BacImplementation.cpp
 * @author Stephan Kreutzer
 * @since 2020-09-24
 */

#include "BacImplementation.h"
#include <sstream>
#include <iostream>



long ParseNumber(const std::string& strInput);



int AD(const std::list<std::unique_ptr<std::string>>& aArguments, std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("Command \"AD\" with pResult != nullptr.");
    }

    if (aArguments.size() != 2)
    {
        throw new std::runtime_error("Command \"AD\" without 2 arguments.");
    }

    long nLhs = 0L;
    long nRhs = 0L;

    std::list<std::unique_ptr<std::string>>::const_iterator iter = aArguments.begin();

    nLhs = ParseNumber(**iter);
    iter++;
    nRhs = ParseNumber(**iter);

    long nResult = nLhs + nRhs;

    std::stringstream strOut;
    strOut << nResult;

    pResult.reset(new std::string);
    *pResult = strOut.str();

    return 0;
}

int SU(const std::list<std::unique_ptr<std::string>>& aArguments, std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("Command \"SU\" with pResult != nullptr.");
    }

    if (aArguments.size() != 2)
    {
        throw new std::runtime_error("Command \"SU\" without 2 arguments.");
    }

    long nLhs = 0L;
    long nRhs = 0L;

    std::list<std::unique_ptr<std::string>>::const_iterator iter = aArguments.begin();

    nLhs = ParseNumber(**iter);
    iter++;
    nRhs = ParseNumber(**iter);

    long nResult = nLhs - nRhs;

    std::stringstream strOut;
    strOut << nResult;

    pResult.reset(new std::string);
    *pResult = strOut.str();

    return 0;
}

int ML(const std::list<std::unique_ptr<std::string>>& aArguments, std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("Command \"ML\" with pResult != nullptr.");
    }

    if (aArguments.size() != 2)
    {
        throw new std::runtime_error("Command \"ML\" without 2 arguments.");
    }

    long nLhs = 0L;
    long nRhs = 0L;

    std::list<std::unique_ptr<std::string>>::const_iterator iter = aArguments.begin();

    nLhs = ParseNumber(**iter);
    iter++;
    nRhs = ParseNumber(**iter);

    long nResult = nLhs * nRhs;

    std::stringstream strOut;
    strOut << nResult;

    pResult.reset(new std::string);
    *pResult = strOut.str();

    return 0;
}

int DV(const std::list<std::unique_ptr<std::string>>& aArguments, std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("Command \"DV\" with pResult != nullptr.");
    }

    if (aArguments.size() != 2)
    {
        throw new std::runtime_error("Command \"DV\" without 2 arguments.");
    }

    long nLhs = 0L;
    long nRhs = 0L;

    std::list<std::unique_ptr<std::string>>::const_iterator iter = aArguments.begin();

    nLhs = ParseNumber(**iter);
    iter++;
    nRhs = ParseNumber(**iter);

    long nResult = nLhs / nRhs;

    std::stringstream strOut;
    strOut << nResult;

    pResult.reset(new std::string);
    *pResult = strOut.str();

    return 0;
}

int PS(const std::list<std::unique_ptr<std::string>>& aArguments, std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("Command \"PS\" with pResult != nullptr.");
    }

    if (aArguments.size() != 1)
    {
        throw new std::runtime_error("Command \"PS\" without 1 argument.");
    }

    std::list<std::unique_ptr<std::string>>::const_iterator iter = aArguments.begin();

    std::cout << **iter;

    pResult.reset(new std::string);

    return 0;
}

int EQ(const std::list<std::unique_ptr<std::string>>& aArguments, std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("Command \"EQ\" with pResult != nullptr.");
    }

    if (aArguments.size() != 4)
    {
        throw new std::runtime_error("Command \"EQ\" without 4 arguments.");
    }

    std::list<std::unique_ptr<std::string>>::const_iterator iterLhs = aArguments.begin();
    std::list<std::unique_ptr<std::string>>::const_iterator iterRhs = ++aArguments.begin();

    if ((**iterLhs) == (**iterRhs))
    {
        pResult.reset(new std::string);
        *pResult = **(++iterRhs);
    }
    else
    {
        ++iterRhs;
        pResult.reset(new std::string);
        *pResult = **(++iterRhs);
    }

    return 0;
}



long ParseNumber(const std::string& strInput)
{
    long nResult = 0L;

    std::stringstream aConverter;
    aConverter << strInput;
    aConverter >> nResult;

    if (aConverter.fail() != false)
    {
        throw new std::runtime_error("Converting string to long failed.");
    }

    return nResult;
}
