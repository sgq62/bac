/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/Segment.h
 * @author Stephan Kreutzer
 * @since 2020-10-24
 */

#ifndef _CPPSTAX_SEGMENT_H
#define _CPPSTAX_SEGMENT_H


#include <string>
#include <vector>
#include <memory>
#include <list>
#include <map>


namespace bac
{

class Segment
{
public:
    Segment(const std::string& strString);

public:
    std::string GetString(const std::vector<std::unique_ptr<std::string>>& aArguments);
    int SetString(const std::string& strString);

    int DrillHoles(const std::list<std::unique_ptr<std::string>>& aStrings);

protected:
    std::string m_strString;
    // <character position, argument index>
    std::unique_ptr<std::map<std::size_t, std::size_t>> m_pHoles;

};

}

#endif
