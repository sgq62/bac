/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/BacInterpreter.cpp
 * @todo Alternative: Reverse Polish notation if reading forward.
 * @author Stephan Kreutzer
 * @since 2020-09-23
 */

#include "BacInterpreter.h"
#include "BacImplementation.h"
#include <sstream>
#include <iomanip>


typedef int (*BacFunction)(const std::list<std::unique_ptr<std::string>>&, std::unique_ptr<std::string>&);


namespace bac
{

BacInterpreter::BacInterpreter(std::istream& aStream):
  m_aStream(aStream)
{
    m_aFunctions.insert(std::pair<std::string, BacFunction>("AD", AD));
    m_aFunctions.insert(std::pair<std::string, BacFunction>("SU", SU));
    m_aFunctions.insert(std::pair<std::string, BacFunction>("ML", ML));
    m_aFunctions.insert(std::pair<std::string, BacFunction>("DV", DV));
    m_aFunctions.insert(std::pair<std::string, BacFunction>("PS", PS));

    //m_aStandby.insert(std::pair<std::string, std::string>("HELLOW", "#(PS, HELLO WORLD!)#(CL, HELLOW)"));
}

BacInterpreter::~BacInterpreter()
{

}

int BacInterpreter::Read(std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("BacInterpreter::Read() with pResult != nullptr.");
    }

    char cByte = '\0';

    do
    {
        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            if (pResult == nullptr)
            {
                pResult.reset(new std::string);
            }

            return 0;
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte == '#')
        {
            std::unique_ptr<std::string> pValue(nullptr);

            HandleCommand(m_aStream, pValue);

            if (pResult == nullptr)
            {
                pResult = std::move(pValue);
            }
            else
            {
                *pResult += *pValue;
            }
        }
        else
        {
            int nByte(cByte);
            std::stringstream aMessage;
            aMessage << "Unexpected character '" << cByte << "' (0x"
                     << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                     << ").";
            throw new std::runtime_error(aMessage.str());
        }

    } while (true);
}

int BacInterpreter::HandleCommand(std::istream& aStream, std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("BacInterpreter::HandleCommand() with pResult != nullptr.");
    }

    char cByte = '\0';

    aStream.get(cByte);

    if (aStream.eof() == true)
    {
        throw new std::runtime_error("Unexpected end of stream at the start of a command.");
    }

    if (aStream.bad() == true)
    {
        throw new std::runtime_error("Stream is bad.");
    }

    if (cByte != '(')
    {
        int nByte(cByte);
        std::stringstream aMessage;
        aMessage << "Unexpected character '" << cByte << "' (0x"
                 << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                 << ") at the start of a command.";
        throw new std::runtime_error(aMessage.str());
    }

    std::stringstream strCommand;
    std::list<std::unique_ptr<std::string>> aArguments;

    do
    {
        aStream.get(cByte);

        if (aStream.eof() == true)
        {
            throw new std::runtime_error("Unexpected end of stream while attempting to read the command name.");
        }

        if (aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (std::isalpha(cByte, m_aLocale) == true)
        {
            strCommand << cByte;
        }
        else if (cByte == ',')
        {
            std::string strCommandString = strCommand.str();

            if (strCommandString.empty() == true)
            {
                throw new std::runtime_error("Command started without name.");
            }

            do
            {
                cByte = ConsumeWhitespace(aStream);

                if (cByte == '\0')
                {
                    throw new std::runtime_error("Unexpected end of stream while consuming whitespace in front of arguments.");
                }

                std::unique_ptr<std::string> pValue(nullptr);

                int nAttributeResult = HandleAttribute(cByte, aStream, pValue);

                if (nAttributeResult == 0)
                {
                    aArguments.push_back(std::move(pValue));
                    continue;
                }
                else if (nAttributeResult == 1)
                {
                    aArguments.push_back(std::move(pValue));
                    break;
                }
                else
                {
                    std::stringstream aMessage;
                    aMessage << "Unexpected return value " << nAttributeResult << " from BacInterpreter::HandleAttribute().";
                    throw new std::runtime_error(aMessage.str());
                }

            } while (true);

            if (aArguments.empty() == true)
            {
                throw new std::runtime_error("Command without arguments.");
            }

            int nResult = ExecuteCommand(strCommandString, aArguments, pResult);

            if (nResult == 0)
            {
                return 0;
            }
            else if (nResult == 1)
            {
                if (pResult->empty() == true)
                {
                    return 0;
                }

                if (pResult->at(0) != '#')
                {
                    return 0;
                }

                std::stringstream strAttribute;
                strAttribute << pResult->substr(1);

                std::unique_ptr<std::string> pValue(nullptr);

                int nAttributeResult = HandleAttribute(pResult->at(0), strAttribute, pValue);

                if (nAttributeResult == 0)
                {
                    throw new std::runtime_error("Use of BacInterpreter::HandleAttribute() to call a command failed.");
                }
                else if (nAttributeResult == 1)
                {
                    pResult = std::move(pValue);
                    return 0;
                }
                else
                {
                    std::stringstream aMessage;
                    aMessage << "Unexpected return value " << nAttributeResult << " from BacInterpreter::HandleAttribute().";
                    throw new std::runtime_error(aMessage.str());
                }
            }
            else
            {
                std::stringstream aMessage;
                aMessage << "Unexpected return value " << nResult << " from BacInterpreter::ExecuteCommand().";
                throw new std::runtime_error(aMessage.str());
            }
        }
        else
        {
            int nByte(cByte);
            std::stringstream aMessage;
            aMessage << "Unexpected character '" << cByte << "' (0x"
                     << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                     << ")bbb.";
            throw new std::runtime_error(aMessage.str());
        }

    } while (true);
}

int BacInterpreter::HandleAttribute(const char& cFirstByte,
                                    std::istream& aStream,
                                    std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("BacInterpreter::HandleAttribute() with pResult != nullptr.");
    }

    if (cFirstByte == '#')
    {
        do
        {
            std::unique_ptr<std::string> pValue(nullptr);

            HandleCommand(aStream, pValue);

            if (pResult == nullptr)
            {
                pResult = std::move(pValue);
            }
            else
            {
                *pResult += *pValue;
            }

            char cByte = ConsumeWhitespace(aStream);

            if (cByte == ',')
            {
                return 0;
            }
            else if (cByte == ')')
            {
                return 1;
            }
            else if (cByte == '#')
            {
                continue;
            }
            else if (cByte == '\0')
            {
                throw new std::runtime_error("Unexpected end of stream while consuming whitespace after an argument.");
            }
            else
            {
                int nByte(cByte);
                std::stringstream aMessage;
                aMessage << "Unexpected character '" << cByte << "' (0x"
                        << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                        << ").";
                throw new std::runtime_error(aMessage.str());
            }

        } while (true);
    }

    std::stringstream strValue;
    strValue << cFirstByte;

    char cByte = '\0';

    do
    {
        aStream.get(cByte);

        if (aStream.eof() == true)
        {
            throw new std::runtime_error("Unexpected end of stream within reading an argument value.");
        }

        if (aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte == ',')
        {
            std::string strValueString = strValue.str();

            if (strValueString.empty() == true)
            {
                throw new std::runtime_error("Attribute end encountered while missing an attribute value.");
            }

            pResult.reset(new std::string);
            *pResult = strValueString;

            return 0;
        }
        else if (cByte == ')')
        {
            std::string strValueString = strValue.str();

            if (strValueString.empty() == true)
            {
                throw new std::runtime_error("Attribute end encountered while missing an attribute value.");
            }

            pResult.reset(new std::string);
            *pResult = strValueString;

            return 1;
        }
        else
        {
            strValue << cByte;
        }

    } while (true);
}

int BacInterpreter::ExecuteCommand(const std::string& strCommand,
                                   const std::list<std::unique_ptr<std::string>>& aArguments,
                                   std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("BacInterpreter::ExecuteCommand() with pResult != nullptr.");
    }

    if (strCommand == "CL")
    {
        if (aArguments.size() != 1)
        {
            throw new std::runtime_error("Command \"CL\" without 1 argument.");
        }

        std::list<std::unique_ptr<std::string>>::const_iterator iterArgument = aArguments.begin();
        std::map<std::string, std::string>::iterator iterStandby = m_aStandby.find(**iterArgument);

        if (iterStandby != m_aStandby.end())
        {
            pResult.reset(new std::string);
            *pResult = iterStandby->second;

            return 1;
        }
        else
        {
            std::stringstream aMessage;
            aMessage << "Unknown standby area name \"" << **iterArgument << "\".";
            throw new std::runtime_error(aMessage.str());
        }
    }
    else if (strCommand == "DS")
    {
        if (aArguments.size() != 2)
        {
            throw new std::runtime_error("Command \"DS\" without 2 arguments.");
        }

        std::list<std::unique_ptr<std::string>>::const_iterator iter = aArguments.begin();
        std::map<std::string, BacFunction>::const_iterator iterFunction = m_aFunctions.find(**iter);

        if (iterFunction != m_aFunctions.end())
        {
            std::stringstream aMessage;
            aMessage << "Can't redefine internal, built-in command \"" << **iter << "\".";
            throw new std::runtime_error(aMessage.str());
        }

        std::string strString(**iter);
        std::string strValue(**(++iter));

        std::map<std::string, std::string>::iterator iterStandby = m_aStandby.find(strString);

        if (iterStandby != m_aStandby.end())
        {
            iterStandby->second = strValue;
        }
        else
        {
            m_aStandby.insert(std::pair<std::string, std::string>(strString, strValue));
        }

        pResult.reset(new std::string);

        return 0;
    }
    else
    {
        std::map<std::string, BacFunction>::iterator iter = m_aFunctions.find(strCommand);

        if (iter != m_aFunctions.end())
        {
            iter->second(aArguments, pResult);
            return 0;
        }
        else
        {
            std::stringstream aMessage;
            aMessage << "Unknown command \"" << strCommand << "\".";
            throw new std::runtime_error(aMessage.str());
        }
    }
}

/**
 * @retval Returns the first non-whitespace character or '\0' in
 *     case of end-of-file.
 */
char BacInterpreter::ConsumeWhitespace(std::istream& aStream)
{
    char cByte('\0');

    do
    {
        aStream.get(cByte);

        if (aStream.eof() == true)
        {
            return '\0';
        }

        if (aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (std::isspace(cByte, m_aLocale) == 0)
        {
            return cByte;
        }

    } while (true);
}

}
