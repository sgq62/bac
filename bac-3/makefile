# Copyright (C) 2020  Stephan Kreutzer
#
# This file is part of BAC.
#
# BAC is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# BAC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with BAC. If not, see <http://www.gnu.org/licenses/>.



.PHONY: build clean



CFLAGS = -std=c++11 -Wall -Werror -Wextra -pedantic



build: bac



bac: bac.cpp BacInterpreter.o BacImplementation.o
	g++ bac.cpp BacInterpreter.o BacImplementation.o -o bac $(CFLAGS)

BacInterpreter.o: BacInterpreter.h BacInterpreter.cpp
	g++ BacInterpreter.cpp -c $(CFLAGS)

BacImplementation.o: BacImplementation.h BacImplementation.cpp
	g++ BacImplementation.cpp -c $(CFLAGS)

clean:
	rm -f ./bac
	rm -f ./bac.o
	rm -f ./BacInterpreter.o
	rm -f ./BacImplementation.o
