/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/bac.cpp
 * @brief Demo program.
 * @author Stephan Kreutzer
 * @since 2020-09-23
 */

#include "BacInterpreter.h"
#include <memory>
#include <iostream>
#include <fstream>


int Run(std::istream& aStream);


int main(int argc, char* argv[])
{
    std::cout << "BAC Copyright (C) 2020 Stephan Kreutzer\n"
              << "This program comes with ABSOLUTELY NO WARRANTY.\n"
              << "This is free software, and you are welcome to redistribute it\n"
              << "under certain conditions. See the GNU Affero General Public License 3\n"
              << "or any later version for details. Also, see the source code repository\n"
              << "https://gitlab.com/skreutzer/bac/ and the project\n"
              << "website https://hypertext-systems.org.\n"
              << std::endl;

    std::unique_ptr<std::ifstream> pStream = nullptr;

    try
    {
        if (argc >= 2)
        {
            pStream = std::unique_ptr<std::ifstream>(new std::ifstream);
            pStream->open(argv[1], std::ios::in | std::ios::binary);

            if (pStream->is_open() != true)
            {
                std::cerr << "Couldn't open input file \"" << argv[1] << "\".";
                return -1;
            }

            Run(*pStream);

            pStream->close();
        }
        else
        {
            std::cout << "Please enter your program, confirm with Ctrl+D:" << std::endl;
            Run(std::cin);
        }
    }
    catch (std::exception* pException)
    {
        std::cerr << "Exception: " << pException->what() << std::endl;

        if (pStream != nullptr)
        {
            if (pStream->is_open() == true)
            {
                pStream->close();
            }
        }

        return -1;
    }

    return 0;
}

int Run(std::istream& aStream)
{
    bac::BacInterpreter aInterpreter(aStream);
    std::unique_ptr<std::string> pResult(nullptr);

    int nResult = aInterpreter.Read(pResult);

    if (nResult == 0)
    {
        std::cout << "\n\n" << *pResult << std::endl;
    }
    else if (nResult == -1)
    {
        std::cerr << "No input." << std::endl;
    }
    else
    {

    }

    return 0;
}
