/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/BacInterpreter.cpp
 * @todo Alternative: Reverse Polish notation if reading forward.
 * @author Stephan Kreutzer
 * @since 2020-09-23
 */

#include "BacInterpreter.h"
#include "BacImplementation.h"
#include <sstream>
#include <iomanip>


typedef int (*BacFunction)(const std::list<std::unique_ptr<std::string>>&, std::unique_ptr<std::string>&);


namespace bac
{

BacInterpreter::BacInterpreter(std::istream& aStream):
  m_aStream(aStream)
{
    m_aFunctions.insert(std::pair<std::string, BacFunction>("AD", AD));
    m_aFunctions.insert(std::pair<std::string, BacFunction>("SU", SU));
    m_aFunctions.insert(std::pair<std::string, BacFunction>("ML", ML));
    m_aFunctions.insert(std::pair<std::string, BacFunction>("DV", DV));
}

BacInterpreter::~BacInterpreter()
{

}

int BacInterpreter::Read(std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("BacInterpreter::Read() with pResult != nullptr.");
    }

    char cByte = '\0';

    do
    {
        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            return -1;
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte == '#')
        {
            HandleCommand(pResult);

            m_aStream.get(cByte);

            if (m_aStream.eof() == true)
            {
                return 0;
            }

            if (m_aStream.bad() == true)
            {
                throw new std::runtime_error("Stream is bad.");
            }

            int nByte(cByte);
            std::stringstream aMessage;
            aMessage << "Unexpected character '" << cByte << "' (0x"
                    << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                    << ") after end of input.";
            throw new std::runtime_error(aMessage.str());
        }
        else
        {
            int nByte(cByte);
            std::stringstream aMessage;
            aMessage << "Unexpected character '" << cByte << "' (0x"
                     << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                     << ").";
            throw new std::runtime_error(aMessage.str());
        }

    } while (true);
}

int BacInterpreter::HandleCommand(std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("BacInterpreter::HandleCommand() with pResult != nullptr.");
    }

    char cByte = '\0';

    m_aStream.get(cByte);

    if (m_aStream.eof() == true)
    {
        throw new std::runtime_error("Unexpected end of stream at the start of a command.");
    }

    if (m_aStream.bad() == true)
    {
        throw new std::runtime_error("Stream is bad.");
    }

    if (cByte != '(')
    {
        int nByte(cByte);
        std::stringstream aMessage;
        aMessage << "Unexpected character '" << cByte << "' (0x"
                 << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                 << ") at the start of a command.";
        throw new std::runtime_error(aMessage.str());
    }

    std::stringstream strCommand;
    std::list<std::unique_ptr<std::string>> aArguments;

    do
    {
        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            if (strCommand.str().empty() == true)
            {
                throw new std::runtime_error("Unexpected end of stream before the command name could be read.");
            }
            else
            {
                throw new std::runtime_error("Unexpected end of stream while attempting to read the command name.");
            }
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (std::isalpha(cByte, m_aLocale) == true)
        {
            strCommand << cByte;
        }
        else if (cByte == ',')
        {
            std::string strCommandString = strCommand.str();
            if (strCommandString.empty() == true)
            {
                throw new std::runtime_error("Command started without name.");
            }

            do
            {
                cByte = ConsumeWhitespace();

                if (cByte == '\0')
                {
                    throw new std::runtime_error("Unexpected end of stream while consuming whitespace in front of arguments.");
                }

                std::unique_ptr<std::string> pValue(nullptr);

                int nAttributeResult = HandleAttribute(cByte, pValue);

                if (nAttributeResult == 0)
                {
                    aArguments.push_back(std::move(pValue));
                    continue;
                }
                else if (nAttributeResult == 1)
                {
                    aArguments.push_back(std::move(pValue));
                    break;
                }
                else
                {
                    std::stringstream aMessage;
                    aMessage << "Unexpected return value " << nAttributeResult << " from BacInterpreter::HandleAttribute().";
                    throw new std::runtime_error(aMessage.str());
                }

            } while (true);

            if (aArguments.empty() == true)
            {
                throw new std::runtime_error("Command without arguments.");
            }

            ExecuteCommand(strCommandString, aArguments, pResult);

            return 0;
        }
        else
        {
            int nByte(cByte);
            std::stringstream aMessage;
            aMessage << "Unexpected character '" << cByte << "' (0x"
                     << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                     << ").";
            throw new std::runtime_error(aMessage.str());
        }

    } while (true);
}

int BacInterpreter::HandleAttribute(const char& cFirstByte, std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("BacInterpreter::HandleAttribute() with pResult != nullptr.");
    }

    if (cFirstByte == '#')
    {
        std::unique_ptr<std::string> pValue(nullptr);

        HandleCommand(pValue);

        pResult = std::move(pValue);

        char cByte = ConsumeWhitespace();

        if (cByte == ',')
        {
            return 0;
        }
        else if (cByte == ')')
        {
            return 1;
        }
        else if (cByte == '\0')
        {
            throw new std::runtime_error("Unexpected end of stream while consuming whitespace after an argument.");
        }
        else
        {
            int nByte(cByte);
            std::stringstream aMessage;
            aMessage << "Unexpected character '" << cByte << "' (0x"
                     << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                     << ").";
            throw new std::runtime_error(aMessage.str());
        }
    }

    std::stringstream strValue;
    strValue << cFirstByte;

    char cByte = '\0';

    do
    {
        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            throw new std::runtime_error("Unexpected end of stream within reading an argument value.");
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte == ',')
        {
            std::string strValueString = strValue.str();

            if (strValueString.empty() == true)
            {
                throw new std::runtime_error("Attribute end encountered while missing an attribute value.");
            }

            pResult.reset(new std::string);
            *pResult = strValueString;

            return 0;
        }
        else if (cByte == ')')
        {
            std::string strValueString = strValue.str();
            if (strValueString.empty() == true)
            {
                throw new std::runtime_error("Attribute end encountered while missing an attribute value.");
            }

            pResult.reset(new std::string);
            *pResult = strValueString;

            return 1;
        }
        else
        {
            strValue << cByte;
        }

    } while (true);

    return 0;
}

int BacInterpreter::ExecuteCommand(const std::string& strCommand,
                                   const std::list<std::unique_ptr<std::string>>& aArguments,
                                   std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("BacInterpreter::ExecuteCommand() with pResult != nullptr.");
    }

    std::map<std::string, BacFunction>::iterator iter = m_aFunctions.find(strCommand);

    if (iter != m_aFunctions.end())
    {
        iter->second(aArguments, pResult);
        //pResolvedText = std::unique_ptr<std::string>(new std::string(iter->second));

        return 0;
    }
    else
    {
        std::stringstream aMessage;
        aMessage << "Unknown command \"" << strCommand << "\".";
        throw new std::runtime_error(aMessage.str());
    }
}

/**
 * @retval Returns the first non-whitespace character or '\0' in
 *     case of end-of-file.
 */
char BacInterpreter::ConsumeWhitespace()
{
    char cByte('\0');

    do
    {
        m_aStream.get(cByte);

        if (m_aStream.eof() == true)
        {
            return '\0';
        }

        if (m_aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (std::isspace(cByte, m_aLocale) == 0)
        {
            return cByte;
        }

    } while (true);
}

}
