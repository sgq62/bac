/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of BAC.
 *
 * BAC is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * BAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with BAC. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/BacInterpreter.h
 * @author Stephan Kreutzer
 * @since 2020-09-23
 */

#ifndef _CPPSTAX_BACINTERPRETER_H
#define _CPPSTAX_BACINTERPRETER_H

#include <istream>
#include <string>
#include <locale>
#include <memory>
#include <list>
#include <map>

namespace bac
{

class BacInterpreter
{
public:
    BacInterpreter(std::istream& aStream);
    ~BacInterpreter();

public:
    int Read(std::unique_ptr<std::string>& pResult);

protected:
    int HandleCommand(std::unique_ptr<std::string>& pResult);
    int HandleAttribute(const char& cFirstByte, std::unique_ptr<std::string>& pResult);

protected:
    int ExecuteCommand(const std::string& strCommand,
                       const std::list<std::unique_ptr<std::string>>& aArguments,
                       std::unique_ptr<std::string>& pResult);

protected:
    char ConsumeWhitespace();

protected:
    std::map<std::string, int (*)(const std::list<std::unique_ptr<std::string>>&, std::unique_ptr<std::string>&)> m_aFunctions;

protected:
    std::istream& m_aStream;
    std::locale m_aLocale;

};

}

#endif
